%% calculate the moment of inertial using bifilar method
%
% Details can be found at the link below
% https://www.mathworks.com/company/newsletters/articles/improving-mass-moment-of-inertia-measurements.html 
%

clear all; close all; clc;


%% x axis
l = 0.41;       %% The length of the support wiree length of the support wire
b = 0.22/2;    %% The distance between two support wires
T = 30/29;      %% A full period of the ocillation in s, we will measure 30 times to obtain the average
g = 9.781;
m = 1.5115;      %% mass

Ix = m*g*T^2*b^2/(4*pi^2*l)

%% y axis
l = 0.48;
b = 0.21/2;
T = 30/25;

Iy = m*g*T^2*b^2/(4*pi^2*l)

%% z axis
l = 0.51;
b = 0.19/2;
T = 30/20;

Iz = m*g*T^2*b^2/(4*pi^2*l)
